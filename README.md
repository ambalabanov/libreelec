# LibreELEC RaspberryPi 3

## Build

Write LibreELEC-RPi2.arm-8.2.2.img to SD-card

## Install Docker

Install Docker from Add-ons/Services

## Install Transmission-docker

    $ wget https://gitlab.com/ambalabanov/libreelec/raw/master/transmission/install.sh
    $ /bin/bash install.sh && rm -fr install.sh

## Install Noxbit-docker
    $ wget https://gitlab.com/ambalabanov/libreelec/raw/master/noxbit/install.sh
    $ /bin/bash install.sh && rm -fr install.sh

## Install Acestream-docker
    $ wget https://gitlab.com/ambalabanov/libreelec/raw/master/acestream/install.sh
    $ /bin/bash install.sh && rm -fr install.sh

## Add SerperRepo repository

Add source "http://srp.nu"

Install from zip file

Allow installation of add-ons from unknown source

Instal from "superrepo.kodi.krypton.all-0.7.04.zip"

## Install Add-ons

Add-ons/SuperRepo All/Video add-ons/IVI

Add-ons/SuperRepo All/Video add-ons/Torrent-TV XBMC
