#!/bin/sh

cd /storage
mkdir acestream-docker
cd acestream-docker
wget https://gitlab.com/ambalabanov/libreelec/raw/master/acestream/acestream.service
systemctl enable /storage/acestream-docker/acestream.service
systemctl start acestream.service