#!/bin/sh

cd /storage
wget https://github.com/ambalabanov/libreelec/archive/master.zip
unzip master.zip
cd libreelec-master/noxbit
docker build . -t ambalabanov/armhf-noxbit
rm -rf /storage/master.zip /storage/libreelec-master/