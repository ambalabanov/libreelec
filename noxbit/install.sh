#!/bin/sh

cd /storage
mkdir noxbit-docker
cd noxbit-docker
wget https://gitlab.com/ambalabanov/libreelec/raw/master/noxbit/noxbit.service
systemctl enable /storage/noxbit-docker/noxbit.service
systemctl start noxbit.service