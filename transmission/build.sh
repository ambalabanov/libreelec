#!/bin/sh

cd /storage
wget https://github.com/ambalabanov/libreelec/archive/master.zip
unzip master.zip
cd libreelec-master/transmission
docker build . -t ambalabanov/armhf-transmission
rm -rf /storage/master.zip /storage/libreelec-master/