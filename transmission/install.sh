#!/bin/sh

cd /storage
mkdir transmission-docker
cd transmission-docker
wget https://gitlab.com/ambalabanov/libreelec/raw/master/transmission/transmission.service
systemctl enable /storage/transmission-docker/transmission.service
systemctl start transmission.service